<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Post
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 * @Vich\Uploadable
 * @ORM\Table(name="post")
 */
class Post
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     *
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     *
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     *
     */
    private $image;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="image_file", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    public function __construct()
    {
        $this->likers = new ArrayCollection();
    }

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="likedPosts")
     */
    private $likers;


    /**
     * @param User
     * @return Post
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $image
     * @return Post
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     * @return Post
     */
    public function setImageFile(File $image =null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikers()
    {
        return $this->likers;
    }

    /**
     * @param User $user
     */
    public function addLiker(User $user)
    {
        $this->likers->add($user);
    }

    /**
     * @param User $user
     */
    public function removeLiker(User $user)
    {
        $this->likers->removeElement($user);
    }

    /**
     * @return integer
     */
    public function count()
    {
        return count($this->likers);
    }

    /**
     * @param User $user
     * @return boolean
     */
    public function hasLiker(User $user)
    {
        return $this->likers->contains($user);
    }

    /**
     * @param string $description
     * @return Post
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

}