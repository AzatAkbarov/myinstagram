<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Post", mappedBy="author")
     */
    private $posts;


    /**
     *
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="likers")
     */
    private $likedPosts;

    /**
     *
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="followers")
     */
    private $follows;


    /**
     *
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="follows")
     */
    private $followers;

    public function __construct()
    {
        parent::__construct();
        $this->posts = new ArrayCollection();
        $this->likedPosts = new ArrayCollection();
        $this->follows = new ArrayCollection();
        $this->followers = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
    }

    /**
     * @param Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getLikedPosts()
    {
        return $this->likedPosts;
    }

    /**
     * @param Post $post
     */
    public function addLikedPost(Post $post)
    {
        $this->likedPosts->add($post);
    }

    /**
     * @param Post $post
     */
    public function removeLikedPost(Post $post)
    {
        $this->likedPosts->removeElement($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getFollows()
    {
        return $this->follows;
    }

    /**
     * @return ArrayCollection
     */
    public function getFollowers()
    {
        return $this->followers;
    }


    /**
     * @param User $user
     */
    public function addFollow(User $user)
    {
        $this->follows->add($user);
    }

    /**
     * @param User $user
     */
    public function removeFollow(User $user)
    {
        $this->follows->removeElement($user);
    }


    /**
     * @param User $user
     */
    public function addFollower(User $user)
    {
        $this->followers->add($user);
    }

    /**
     * @param User $user
     */
    public function removeFollower(User $user)
    {
        $this->followers->removeElement($user);
    }

    /**
     * @param User $user
     * @return boolean
     */
    public function hasFollow(User $user)
    {
        return $this->follows->contains($user);
    }
}