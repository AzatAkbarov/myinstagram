<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/instagram")
 */
class PostController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     */
    public function indexAction()
    {
        if($this->getUser()) {
            return $this->redirectToRoute('app_post_showallposts');
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    /**
     * @Route("/myProfile")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showMyProfile(Request $request)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $post = new Post();

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $post->setAuthor($currentUser);
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('app_post_showmyprofile');
        }

        $myFollows = $currentUser->getFollows();
        $myPosts = $currentUser->getPosts();
        $postsMyFollows = [];
        foreach ($myFollows as $myFollow) {
            $postsThisFollow = $myFollow->getPosts();
            foreach ($postsThisFollow as $post) {
                $postsMyFollows[] = $post;
            }
        }
        return $this->render('@App/Post/myProfile.html.twig', array(
            "form" => $form->createView(),
            "myPosts" => $myPosts,
            "postMyFollows" => $postsMyFollows
        ));
    }

    /**
     * @Route("/posts")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAllPosts(Request $request)
    {
        $allPosts = $this->getDoctrine()->getRepository(Post::class)->findAll();
        return $this->render('@App/Post/allPosts.html.twig', array(
            "posts" => $allPosts,
            "currentUser" => $this->getUser()
        ));
    }


    /**
     * @Route("/like/{id}", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addLikeOnPost(int $id, Request $request)
    {
        $currentUser = $this->getUser();
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
            if(!($post->hasLiker($currentUser))) {
                $post->addLiker($currentUser);
            } else {
                $post->removeLiker($currentUser);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }

    /**
     * @Route("/follow/{id}", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function subscribeToAuthor(int $id,Request $request)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $personIWantToSubscribe = $this->getDoctrine()->getRepository(User::class)->find($id);
        if($currentUser->getId() !== $id) {
            if(!($currentUser->hasFollow($personIWantToSubscribe))) {
                $personIWantToSubscribe->addFollower($currentUser);
            } else {
                $personIWantToSubscribe->removeFollower($currentUser);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($personIWantToSubscribe);
            $em->flush();
        }
        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }

}