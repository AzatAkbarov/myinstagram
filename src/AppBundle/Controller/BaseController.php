<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 5/29/18
 * Time: 5:58 PM
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     */
    public function indexAction()
    {
        if($this->getUser()) {
            return $this->redirectToRoute('app_post_showallposts');
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

}